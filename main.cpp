#include <QApplication>
#include <QPushButton>
#include <QPixmap>
#include <QPainter>
#include <QPaintEvent>
#include <QTimer>

#include <QMediaPlayer>
#include <QMediaContent>

#include <iostream>

class ImageButton : public QPushButton
{
    Q_OBJECT
public:
    ImageButton() = default;
    ImageButton(QWidget* parent);
    void paintEvent(QPaintEvent *e) override;
    void keyPressEvent(QKeyEvent *e) override;
    QSize sizeHint() const override;
    QSize minimumSizeHint() const override;

public slots:
    void setUp();
    void setDown();
private:
    QPixmap mCurrentButtonPixmap;
    QPixmap mButtonDownPixmap;
    QPixmap mButtonUpPixmap;
    bool isDown = false;
};

ImageButton::ImageButton(QWidget *parent)
{
    setParent(parent);
    setToolTip("Stop");
    setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    mButtonUpPixmap = QPixmap("D:/QtProjects/myWidget/Up.png");
    mButtonDownPixmap = QPixmap("D:/QtProjects/myWidget/Down.png");
    mCurrentButtonPixmap = mButtonDownPixmap;
    setGeometry(mCurrentButtonPixmap.rect());
    connect(this, &QPushButton::clicked,this,&ImageButton::setDown);
}

void ImageButton::paintEvent(QPaintEvent *e)
{
    QPainter p(this);
    p.drawPixmap(e->rect(), mCurrentButtonPixmap);
}

QSize ImageButton::sizeHint() const
{
    return QSize(100,100);
}

QSize ImageButton::minimumSizeHint() const
{
    return sizeHint();
}

void ImageButton::keyPressEvent(QKeyEvent *e)
{
    setDown();
}

void ImageButton::setUp()
{
    mCurrentButtonPixmap = mButtonUpPixmap;
    update();
}

void ImageButton::setDown()
{
    mCurrentButtonPixmap = mButtonDownPixmap;
    QTimer::singleShot(100,this,&ImageButton::setUp);
    update();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QWidget playerWindow;
    auto *player = new QMediaPlayer(&playerWindow);
    ImageButton redButton(nullptr);
    redButton.setFixedSize(300,300);
    QObject::connect(&redButton,&QPushButton::clicked, [](){std::cout<<"click\n";});

    QString filePath {"D:/QtProjects/myWidget/Button_sound.mp3"};
    bool isPlaying = false;
    QObject::connect(&redButton,&QPushButton::clicked, [&filePath,player,&isPlaying]()
        {
            if (!isPlaying)
            {
                player->setMedia(QUrl::fromLocalFile(filePath));
                player->setVolume(70);
        }
        isPlaying = true;
        player->play();
    });

    redButton.show();
    return a.exec();
}

#include <main.moc>
